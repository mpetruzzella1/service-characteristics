# The _data-types_ Folder
This folder contains all classes that describe concepts within the Trust Framework that either do not align to any other folder's convention or that do not warrant a creation of, e.g., a class of the same significance of an Entity of the Gaia-X Conceptual Model (such as _Resource_, _ServiceOffering_, or _Participant_).

Should a datatype gain in importance in the model to finally warrant a description as another _gax-*_ class, the corresponding yaml file must be migrated to the appropriate folder along with appropriate changes to all references to it. 