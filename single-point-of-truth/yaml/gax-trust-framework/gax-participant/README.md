# The _gax-participant_ Folder
This folder contains subclasses of the _Participant_ class defined in the Gaia-X Conceptual Model and described in the _gax-core_ folder hierarchy. 