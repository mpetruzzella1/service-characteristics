# Validating compliance - wip

This page illustrates a proposition of concepts and workflows that need to be reviewed and validated by the Gaia-X Technical Commitee.

The `gax-compliance` namespace introduces several types of self-descriptions to manage the compliance of Service Offerings.

```mermaid
classDiagram

ComplianceAssessmentBody "1" -- "*" ThirdPartyComplianceCertificateClaim
ComplianceAssessmentBody "1" -- "*" ThirdPartyComplianceCertificateCredential
ComplianceAssessmentBody "*" -- "*" ThirdPartyComplianceCertificationScheme
ComplianceAssessmentBody  <|-- Participant: Inheritance
class ComplianceAssessmentBody{
  List~ThirdPartyComplianceCertificationScheme canCertifyThirdPartyComplianceCertificationScheme
  List~ThirdPartyComplianceCertificateClaim hasThirdPartyComplianceCertificateClaim
  List~ThirdPartyComplianceCredential hasThirdPartyComplianceCredential
}


class Participant

ComplianceReferenceManager  <|-- Participant: Inheritance
ComplianceReferenceManager "1" -- "*" ComplianceReference
class ComplianceReferenceManager{
  List~ComplianceReference~ hasComplianceReferences
}

ThirdPartyComplianceCertificationScheme <|-- ComplianceCertificationScheme: Inheritance
class ThirdPartyComplianceCertificationScheme{
 List~ComplianceAssessmentBody~ hasComplianceAssessmentBody
}

ComplianceReference "1" -- "*" ComplianceCertificationScheme
class ComplianceReference{
  xsd:anyURI hasReferenceUrl
  xsd:string hasSha256
  xsd:string hasComplianceReferenceTitle
  xsd:string hasDescription
  ComplianceReferenceManager hasComplianceReferenceManager
  xsd:string hasVersion
  xsd:dateTime cRValidFrom
  xsd:dateTime cRValidUntil
  List~ComplianceCertificationScheme hasComplianceCertificationSchemes
}

ComplianceCriteriaCombination -- "*" ComplianceCriterion
class ComplianceCriteriaCombination{
  xsd:string hasName
  xsd:string hasDescription
  List~ComplianceCriterion requiredOrGrantedCriteria
}

class ComplianceCriterion{
  xsd:string hasName
  xsd:string hasDescription
  xsd:string hasLevel
}

ComplianceLabel -- "*" ComplianceCriteriaCombination
class ComplianceLabel{
  xsd:string hasName
  xsd:string hasDescription
  xsd:string hasLevel
  List~ComplianceCriteriaCombination hasRequiredCriteriaCombinations
}

ComplianceCertificationScheme "*" --o "1" ComplianceReference
ComplianceCertificationScheme "*" -- "1" ComplianceCriteriaCombination
class ComplianceCertificationScheme{
 ComplianceReference hasComplianceReference
 ComplianceCriteriaCombination grantsComplianceCriteriaCombination
}

ComplianceCertificateClaim "*" -- "1" ComplianceCertificationScheme
ComplianceCertificateClaim "*" -- "1" LocatedServiceOffering
class ComplianceCertificateClaim{
 ComplianceCertificationScheme hasComplianceCertificationScheme
 LocatedServiceOffering hasServiceOffering
}

ComplianceCertificateCredential <|-- VerifiableCredential: Inheritance
ComplianceCertificateCredential "1" -- "1" ComplianceCertificateClaim
class ComplianceCertificateCredential{
 ComplianceCertificateClaim credentialSubject
}

ThirdPartyComplianceCertificateCredential <|-- ComplianceCertificateCredential: Inheritance
ThirdPartyComplianceCertificateCredential "1" -- "1" ThirdPartyComplianceCertificateClaim
class ThirdPartyComplianceCertificateCredential{
  ThirdPartyComplianceCertificateClaim credentialSubject
}

ThirdPartyComplianceCertificateClaim <|-- ComplianceCertificateClaim
class ThirdPartyComplianceCertificateClaim{
  ComplianceAssessmentBody hasComplianceAssessmentBody
}

```



## Self descriptions for compliance

### ComplianceReferenceManager

This actor is a Participant inside a given Gaia-X ecosystem in charge of emitting VCs for ComplianceReference and subclasses of ComplianceCertificationScheme.

Several ComplianceReferenceManager can be involved in the same Gaia-X ecosystem. Yet a ComplianceReferenceManager has authority only on the ComplianceReference instances it has created as well as the ComplianceCertificationScheme referencing the ComplianceReference.

**_RULE_** `issuer` of a `ComplianceCertificationScheme` VerifiableCredential equals `hasComplianceReference`.`hasComplianceReferenceManager`.

Each ComplianceReferenceManager must be validated and approved by the **Gaia-X Trust Framework Service** (ComplianceReferenceManager VerifiableCredential is signed by https://compliance.gaia-x.eu/).

To facilitate the bootstrap of compliance features, the default ComplianceReferenceManager is the **Gaia-X Trust Framework Service** itself.

### ComplianceReference

This object represents a set of documentation norms and standards, services must comply with. For example: SecNumCloud.

A ComplianceReference is composed of mandatory attributes like: the URI of reference document related to the compliance, the title, description version and validity dates of the reference.

Each ComplianceReference is signed by a ComplianceReferenceManager. 
This relationship is indicated through:
* the `hasComplianceReferenceManager` attribute of the ComplianceReference
* the `issuer` attribute of the created ComplianceReference VerifiableCredential

**_RULE_** `hasComplianceReferenceManager` equals `issuer` for ComplianceReference VerifiableCredential

### ComplianceCertificationScheme

This object declares a way to certify a Service Offering from a Participant has a ComplianceReference. 

It is an **abstract** class as concrete certification schemes (subclass instances) must be created by the ComplianceReferenceManager in charge of the ComplianceReference.

According to the importance of a ComplianceReference for a Gaia-X ecosystem and the ability to establish controls by humans, different schemes of certification can be allowed. At the time of this writing, only **self-assessment** and **3rd party certification** (which involves a **compliance assessment body**) are possible but other schemes could be added later.

A ComplianceCertificationScheme is associated to a ComplianceCriteriaCombination to indicate the granted criterion.

**_RULE_** `issuer` for ComplianceCertificationScheme VerifiableCredential equals `hasComplianceReference.hasComplianceReferenceManager`

### ComplianceAssessmentBody

`ComplianceAssessmentBody` is a role assumed by a Participant inside a given Gaia-X ecosystem. 

Functionally, a **compliance assessment body** is trusted to assert Service Offerings provided by Providers have been certified for a given compliance in the case of a ThirdPartyComplianceCertificationScheme.

### ThirdPartyComplianceCertificationScheme

This object is a concrete certification scheme that allows an unordered list of ComplianceAssessmentBody role to claim a service offering provided by a Provider is certified.

A ThirdPartyComplianceCertificationScheme instance is signed by a ComplianceReferenceManager.

**_RULE_** The value of `issuer` for ThirdPartyComplianceCertificationScheme VerifiableCredential equals the value of `hasComplianceReference.hasComplianceReferenceManager`

### ComplianceCertificateClaim

This object claims a service offering has been certified for a compliance.

It associates a ComplianceCertificationScheme with a ServiceOffering.

**_RULE_** for a Self-Assessed claim: the value of `issuer` of VerifiableCredential equals the value of `hasServiceOffering.providedBy`

### ThirdPartyComplianceCertificateClaim

In addition to the inherited ComplianceCertificateClaim, a reference to the **compliance assessment body** is provided.

**_RULE_** for a ThirdPartyComplianceCertificateClaim claim: `issuer` of VerifiableCredential equals `hasComplianceAssessmentBody`

### ComplianceCertificateCredential

Is a VerifiableCredential self signed by a Provider in the case of self-assessed labelling criterion.

### ThirdPartyComplianceCertificateCredential

Is a VerifiableCredential signed by a ComplianceAssessmentBody in the case of audited criterion.

### ComplianceCriteriaCombination

Is a list of ComplianceCriterion to be granted for the labelling or compliance. All the criteria have to be passed (logic AND).

### ComplianceCriterion

Refers to a criterion defined in the Gaia-X Trust Framework or Labelling Criteria.
### ComplianceLabel

Refers to a label defined in the Gaia-X Labelling Framework.
A label has a level and a several ComplianceCriteriaCombination. Any ComplianceCriteriaCombination passed grants the referencing ComplianceLabel.

## Example of ThirdParty compliance certification

Initial setup

* A Participant CompRefManager is considered as a valid ComplianceReferenceManager by the Gaia-X Trust Framework service. Thus Participant CompRefManager has a ComplianceReferenceManager VC signed by https://compliance.gaia-x.eu/
* Participant ProviderA has been onboarded into the Ecosystem. Thus ProviderA has a Provider VC signed by https://compliance.gaia-x.eu/.
* Participant AUDITOR has been onboarded into the Ecosystem. Thus AUDITOR has a ComplianceAssessmentBody VC signed by https://compliance.gaia-x.eu/.
* Participant ProviderA has registered a service offering name IAAS. This service if located in France. Thus a LocatedServiceOffering VC named IAAS-FR has been signed by https://compliance.gaia-x.eu/ .

A new Compliance ISO-27001 must be supported inside the ecosystem:

* Participant CompRefManager signs a ComplianceReference VC
* Participant CompRefManager evaluates the criticity of ISO-27001 compliance. It decides to rely on a network of compliance assessment bodies to trust assertion of certification compliance. ThirdPartyComplianceCertificationScheme is the only way to justify a service is ISO-27001 certified.
* Participant AUDITOR is considered as a valid compliance assessment body for ISO-27001 by the CompRefManager. Thus CompRefManager signs a ThirdPartyComplianceCertificationScheme VC which indicates Participant AUDITOR is allowed to claim ISO-27001 compliance.

Participant ProviderA wants to claim its service IAAS is ISO-27001 compliant:

* ISO-27001 compliance must be assessed. Those steps are out of Gaia-X.
  * ProviderA contact AUDITOR to asks ISO-27001 assessment for its IAAS service
  * AUDITOR runs assessment
* if AUDITOR considers service IAAS is ISO-27001 compliant
  * it creates and signs a ThirdPartyComplianceCertificateClaim IAAS-ISO-27001 indicating the IAAS service offering, AUDITOR and the ThirdPartyComplianceCertificationScheme
  * it creates and signs a ThirdPartyComplianceCertificateCredential referencing the ThirdPartyComplianceCertificateClaim IAAS-ISO-27001
