from datetime import datetime
import os
import re
import typing
import yaml2mermaid


def _get_version() -> str:
    revision_state = datetime.now()
    return revision_state.strftime("%d/%m/%Y")


def _generate_outline(all_yaml_file_definitions: dict) -> str:
    """
    Returns markdown outline for all yaml classes.

    @param all_yaml_file_definitions: Dict of all yaml files to be converted to markdown.
    @type all_yaml_file_definitions: dict

    :return: markdown outline as string
    @rtype: str
    """
    output_str = '## Outline\n\n'

    print("-----------------------------------------------------------------")
    print("Generating outline... ")
    print("-----------------------------------------------------------------")

    for key in all_yaml_file_definitions.keys():
        print("..." + key)
        # output_str += "- [" + class_name + "](#" + key.lower() + ")\n"
        output_str += "- [" + key + "](#" + key.lower() + ")\n"

    return output_str + "\n"


def _generate_title(full_class_name: str) -> str:
    output_str = "<div id=\"" + full_class_name.lower() + "\"></div>\n\n\n"
    return output_str + "## " + full_class_name + "\n\n"


def _generate_super_classes(yaml_sef_of_class: dict) -> str:
    try:
        super_classes = yaml_sef_of_class['subClassOf']
    except KeyError:
        super_classes = ''

    return " **Super classes**: " + _convert_list_to_string(super_classes, True) + "\n\n"


def _get_sub_classes(full_class_name: str, all_yaml_file_definitions: dict) -> str:
    sub_classes = []
    for full_sub_class_name in all_yaml_file_definitions.keys():
        yaml_sub_class_definition = \
            all_yaml_file_definitions[full_sub_class_name][full_sub_class_name.split(":")[1]]
        if full_class_name in yaml_sub_class_definition['subClassOf']:
            sub_classes.append(full_sub_class_name)

    return " **Sub classes**: " + _convert_list_to_string(sub_classes, True) + "\n\n"


def _generate_mermaid(full_class_name: str, all_yaml_file_definitions: dict) -> str:
    # generate mermaid
    output_str = ""
    mermaid = yaml2mermaid.draw_mermaid_diagram([full_class_name], all_yaml_file_definitions, draw_attributes=True)
    if mermaid == "classDiagram\n\n\n":
        mermaid += "class " + yaml2mermaid.get_full_mermaid_class_name(full_class_name) + "\n\n"

    output_str += "```mermaid\n"
    output_str += mermaid
    return output_str + "```\n\n"


def _get_attribute_table(full_class_name: str, all_yaml_file_definitions: dict) -> str:

    markdown_table = "\n\n | Title | Data Type | Min Count | Max Count | Description | Example Value | \n" \
                     "| ------- | ------- | ------- | ------- | ------- | ------- | \n" \
                     ""
    ecosystem, class_name = full_class_name.split(':')
    yaml_def_of_class = all_yaml_file_definitions[full_class_name][class_name]
    for a in yaml_def_of_class['attributes']:
        print("   ... parse attribute " + a['title'])
        # parse attribute's title
        markdown_table += "| " + a['title'] + " | "

        # parse attribute's data type
        data_type = str(a['dataType'])
        if data_type in all_yaml_file_definitions.keys():
            markdown_table += "[" + data_type + "](#" + data_type.lower() + ")"
        else:
            markdown_table += data_type

        min_value = ''
        max_value = ''

        try:
            min_value = str(a['minValue']) + " <= "
        except KeyError:
            pass
        try:
            max_value = "=< " + str(a['maxValue'])
        except KeyError:
            pass

        if min_value != '' or max_value != '':
            markdown_table += "<br/>" + min_value + " value " + max_value

        try:
            value_range = a['valueIn']
            markdown_table += "<br/> value must be in: [" + _convert_list_to_string(value_range) + "]"
        except KeyError:
            pass

        markdown_table += " | "

        # parse cardinality
        min_count = "0"
        max_count = "unlimited"
        if 'cardinality' in a.keys():
            if not re.match(r"[0-9]..([0-9]+|\*)", a["cardinality"]):
                raise Exception("The cardinality pattern isn't conform to the definition")
            min_count = a["cardinality"].split('..')[0]
            max_count = a["cardinality"].split('..')[1]
            if max_count == "*":
                max_count = "unlimited"

        markdown_table += min_count + " | "
        markdown_table += max_count + " | "

        # parsing description
        markdown_table += a['description'] + " | "

        # parsing example values
        markdown_table += _convert_list_to_string(a['exampleValues']) + " | \n"

    return markdown_table + "\n"


def _generate_content(all_yaml_file_definitions: dict,
                      draw_class_diagram: bool = False) -> str:
    """
    Returns markdown content for all given classes.

    @param all_yaml_file_definitions: Dict of all yaml files to be converted to markdown.
    @type all_yaml_file_definitions: dict
    @param draw_mermaid If true, mermaid class diagrams are added to markdown. Default: False
    @type draw_class_diagram: bool

    @return: markdown content as string
    @rtype str
    """

    output_str = '' #'## Class\' Descriptions\n\n'

    print("-----------------------------------------------------------------")
    print("Generating content...")
    print("-----------------------------------------------------------------")

    for key in all_yaml_file_definitions.keys():
        class_ecosystem, class_name = key.split(":")
        class_full_yaml_def = all_yaml_file_definitions[key][class_name]
        print("..." + key)
        output_str += "\n"

        output_str += _generate_title(key)
        if draw_class_diagram:
            output_str += _generate_mermaid(key, all_yaml_file_definitions)
        output_str += _generate_super_classes(all_yaml_file_definitions[key][class_name])
        output_str += _get_sub_classes(key, all_yaml_file_definitions)
        output_str += _get_attribute_table(key, all_yaml_file_definitions)

    return output_str + "\n"


def _convert_list_to_string(list_of_strings: typing.List[str], add_link: bool = False) -> str:
    """
    Converts a given list of strings into a string to be inserted in markdown.

    @param list_of_strings: list of strings
    @type list_of_strings: typing.List[str]
    @param add_link: true if link in markdown file should be added
    @type add_link: bool

    @return: list as string
    @rtype: str
    """
    if list_of_strings is None:
        return ""
    string = ""
    for item in list_of_strings:
        if string != "":
            string = string + ", "
        if add_link:
            string = string + "[" + str(item) + "](#" + str(item).lower() + ")"
        else:
            string = string + str(item)

    return string


def _sort_dict(unsorted_dict: dict) -> dict:
    sorted_keys = sorted(unsorted_dict.keys())
    sorted_dict = dict()

    for key in sorted_keys:
        sorted_dict[key] = unsorted_dict[key]

    return sorted_dict


def write_markdown(all_yaml_file_definitions: dict,
                   output_file: str = "yaml2markdown.md",
                   draw_class_diagram: bool = False) -> None:
    """
    Convert given yaml files to markdown and writes this in given file. Markdown file contains:

    - Outline: List of all classes
    - Mermaid: For each given class a mermaid class including super classes, sub classes and referenced classes
    - Attribute Table: A table of attributes for each given class

    @param all_yaml_file_definitions: Dict of all yaml files to be converted to markdown.
    @type all_yaml_file_definitions: dict
    @param output_file: name of output file to write markdown in
    @type output_file: str
    @param draw_mermaid If true, mermaid class diagrams are added to markdown. Default: False
    @type draw_class_diagram: bool
    """

    all_yaml_file_definitions = _sort_dict(all_yaml_file_definitions)

    try:
        # Open a given output file
        output_file = open(os.path.abspath(output_file), "w")
        # Title
        output_file.writelines("# Self Description Model\n\n")
        # Version
        output_file.writelines("Version: " + _get_version() + "\n\n")
        # Outline
        output_file.writelines(_generate_outline(all_yaml_file_definitions))
        # Attribute Tables
        output_file.writelines(_generate_content(all_yaml_file_definitions, draw_class_diagram=draw_class_diagram))
    except Exception as e:
        print(e)
    finally:
        output_file.close()
